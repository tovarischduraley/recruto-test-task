from django.shortcuts import render


def greeting(request):
    context = {
        "name": request.GET.get("name"),
        "message": request.GET.get("message")
    }
    return render(request, "main.html", context=context)
